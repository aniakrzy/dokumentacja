Architektura projektu
============================
Diagram przypadków użycia
----------------------------

.. uml::

	@startuml
	left to right direction
	actor "Użytkownik" as user

    	usecase (Wyświetl wszystkich klientów) as (1)
	usecase (Wyświetl klienta o podanym id) as (2)

	user --> 1
	user --> 2

	@enduml

Diagram klas
--------------------------

.. uml::

   @startuml
   skinparam classAttributeIconSize 0


	class Customer{
	{field}-id:Long
	{field}-firstName:String
	{field}-lastName:String

	{method}+getters
	{method}+toString
	}

   interface CustomerRepoository

   class CustomerConstroller{
                     {field}-customerRepository:CustomerRepository

                     {method}+getCustomers
                     {method}+getUserById
                     }

 
   CustomerRepoository --> CustomerConstroller
   Customer --> CustomerRepoository




   @enduml