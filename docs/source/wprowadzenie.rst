
Czym jest Docker oraz docker-compose?
========================================
Docker
-------
Docker to platforma umożliwająca konteneryzowanie aplikacji. W ramach kontenera możliwe jest stworzenie wirtualnego środowiska, co 
pozwala na uruchamianie stworzonych aplikacji na różnych systemach operacyjnych bez koniczności konfigurowania przez użytkownika np. bibliotek czy wersji zależności.


docker-compose
---------------

Docker-compose pozwala na uruchomnienie więcej niż jednego kontenera na raz w ramach jednej aplikacji. Oznacza to, że można np. używać kontenera zawierającego bazę danych oraz z programu korzystającego z danych tej bazy (np. skryptu tworzącego API), umieszczonego w innym kontenerze, bez konieczności uchuchamiania 
tych serwisów osobno. Można to zrobić jednocześnie, używając konfiguracji w pliku *docker-compose.yaml*
