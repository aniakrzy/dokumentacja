.. lab3 documentation master file, created by
   sphinx-quickstart on Sat Jun 25 13:11:44 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to lab3's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Instrukcja obsługiwania aplikacji
============================
Wymagania
----------------------------
Zainstalowany docker oraz docker-compose


Kroki konfiguracji
----------------------------

- Stwórz projekt z wykorzystaniem strony_.
.. _strony: https://start.spring.io/ `

- Stwórz plik *Dockerfile* w folderze nadrzędnym projeku oraz dodaj jego zawartości:

.. code-block:: java

   FROM gradle:6.0.1-jdk8 AS build
   USER root
   RUN mkdir app
   COPY src/ /app/src/
   COPY build.gradle /app/
   COPY settings.gradle /app/
   WORKDIR /app
   RUN gradle bootJar
   FROM openjdk:8-jdk-alpine
   COPY --from=build /app/build/libs/app.jar app.jar
   ENTRYPOINT ["java","-jar","/app.jar"]


- Stwórz katalog *docker-entrypoint-initdb.d*. oraz dodaj w nim plik konfiguracyjny *create-database.sh*
.. code-block:: java

      set -e
      psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
         CREATE USER docker;
         CREATE DATABASE docker;
         GRANT ALL PRIVILEGES ON DATABASE docker TO docker;
      EOSQL


- Stwórz plik *docker-compose.yaml* oraz dodaj jego zawartość: 

.. code-block:: java

  version: "3"
  services:
      app:
          image: "nazwa_obrazu"
          build:
              context: .
              dockerfile: "Dockerfile"
          environment:
              POSTGRES_USER: ${POSTGRES_USER}
              POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
          ports:
              - 8000:8080
      db:
          image: postgres:latest
          volumes:
              - "./docker-entrypoint-initdb.d:/docker-entrypoint-initdb.d"
          environment:
              POSTGRES_USER: ${POSTGRES_USER}
              POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
          ports:
              - ${DB_PORT}:5432

- Dodaj plik *.env*:

.. code-block:: java

  POSTGRES_USER=postgres
  POSTGRES_PASSWORD=mysecretpass
  DB_PORT=5433

- Dokonaj konfiguracji aplikacji w pliku *application.properties*:

.. code-block:: java

  server.port=8080
  spring.datasource.url=jdbc:postgresql://db:5432/docker
  spring.datasource.username=${POSTGRES_USER}
  spring.datasource.password=${POSTGRES_PASSWORD}
  spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.PostgreSQLDialect
  spring.jpa.hibernate.ddl-auto = update

- Budowanie obrazu za pomocą:
.. code-block:: java

   docker-compose build

- Uruchomienie aplikacji wraz z bazą danych za pomocą komedny:
.. code-block:: java
   docker-compose up