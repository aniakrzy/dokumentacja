.. toctree::
    :maxdepth: 2  ".. głębokość sekcji w generowanym spisie"
    :caption: Zawartość: ".. użyty tytuł"

Opis REST API
======================

Zapytania dostępne dla użytkowników:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- GET *http://localhost:8000/customers/{id}*

- GET *http://localhost:8000/customers*

